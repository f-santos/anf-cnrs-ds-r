## Exécuter à nouveau le notebook 1 :
library(groundhog)
groundhog.library("here", "2021-10-15")
capture.output(source(here("jour2", "01_workflow_jupyter.r")))

## Chargement des données :
analytic <- read.csv(
    here("data", "data_analytic.csv"),
    row.names = 1,
    stringsAsFactors = TRUE
)
head(analytic)

## Nombre de sujets ayant pris part à l'analyse :
nlevels(analytic$name)

## Nombre de sujets par condition "contrôle position" / "contrôle vitesse" :
tapply(analytic$name, INDEX = analytic$ctrl, FUN = function(x) nlevels(factor(x)))

## Créer un dataframe réduit :
reduced <- analytic[, c("name", "ctrl", "force_level")] |>
    unique() |>
    na.omit()
head(reduced)

## Beeswarm du 22%MVF :
options(repr.plot.width = 12, repr.plot.height = 6) # taille du graphique
beeswarm(force_level ~ ctrl, data = reduced, pch = 16,
        main = "Niveau de force (%22MVF) selon la condition expérimentale")
grid()

summary(analytic)

## Conversion en facteur pour la variable `cutoff` :
analytic$cutoff <- factor(analytic$cutoff)
summary(analytic)

## Nombre globale d'essais par sujet :
table(analytic$name)

## Nombre d'essais de chaque type (ent/test) par sujet :
with(analytic, table(name, phase))

## Nombre d'essais par valeur de coupure :
with(analytic, table(name, cutoff))

## Nombre d'essais de type pe/pc :
with(analytic, table(name, feedback))

## Un retour sur la variable `force_level` :
par(mfrow = c(1, 2))
boxplot(analytic$force_level)
kde <- density(analytic$force_level)
plot(kde, main = "Densité estimée")
rug(analytic$force_level)

## Où se situent les valeurs les plus élevées ?
analytic |>
    select(name, force_level) |>
    unique() |>
    arrange(force_level) |>
    tail()

## Une ultime vérification sur l'équilibrage du plan :
aggregate(name ~ cutoff + phase + feedback + ctrl,
          data = analytic, FUN = length)

## Exécuter à nouveau le notebook 1 :
library(groundhog)
groundhog.library("here", "2021-10-15")
source(here("jour2", "01_workflow_jupyter.r"))

## Connaître la version de tous les packages utilisés :
print(sessionInfo(), locale = FALSE)

## Construire le chemin (absolu) d'accès au répertoire des données :
here("data", "sujets")

## Que contient ce répertoire ?
list.files(here("data", "sujets"))

## Un affichage plus élégant, sous forme d'arbre :
dir_tree(here("data", "sujets"), recurse = 0)

## Déterminer le nombre de sujets :
length(list.files(here("data", "sujets")))

## Une autre façon (plus lisible ?) de coder, avec les pipes natifs de R (> 4.1.0) :
here("data", "sujets") |>
    list.files() |>
    length()

## Contenu du répertoire du sujet 3 :
dir_tree(here("data", "sujets", "sujet3"))

## Contenu du répertoire du sujet 8 :
dossier_s8 <- here("data", "sujets", "sujet8")
dir_tree(dossier_s8)

## Nombre de fichiers dans le répertoire du sujet 8 :
dossier_s8 |> list.files() |> length()

## Séparer les composantes du nom des fichiers du sujet 8 :
dossier_s8 |>
    list.files() |>
    gsub(pattern = ".json", replacement = "") |>
    strsplit(split = "_")

## Chargement et résumé d'un fichier de données pour le sujet 8 :
exsuj8 <- fromJSON(
    content = here("data", "sujets", "sujet8", "sujet8_vit_ent_0.2_pc_1.json")
)
summary(exsuj8)

exsuj8$pv_cond

exsuj8$force_level

exsuj8$percent_err

head(exsuj8$pos_ct)

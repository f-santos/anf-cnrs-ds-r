## Exécuter à nouveau le notebook 1 :
library(groundhog)
groundhog.library("here", "2021-10-15")
capture.output(source(here("jour2", "01_workflow_jupyter.r")))

## Extraire l'information de chaque fichier :
extract_analytic_info <-  function(file) {
    ## Lire et extraire les info du *contenu* du fichier :
    values <- fromJSON(file)
    ## Extraire les info du *nom* du fichier :
    path <- unlist(strsplit(file, "/"))
    name_comp <- unlist(strsplit(path[length(path)], "_"))
    ## Rassembler les info en un dataframe :
    data.frame(
        name = name_comp[1],
        force_level = ifelse(is.null(values$force_level[1]), NA, values$force_level[1]),
        ctrl = name_comp[2],
        phase = name_comp[3],
        cutoff = name_comp[4],
        feedback = name_comp[5],
        percent_err = ifelse(is.null(values$percent_err[1]), NA, values$percent_err[1]),
        nb_points = length(values$fb_err),
        num_essai = as.numeric(gsub(x = name_comp[6], pattern = ".json", rep = ""))
    )
}

## Tester l'extraction des informations sur un fichier unique :
extract_analytic_info(
    file = here("data", "sujets", "sujet4", "sujet4_pos_ent_0.2_pc_1.json")
)

## Chemins complets de tous les fichiers JSON :
jsonfiles <- list.files(
    path = here("data", "sujets"),
    recursive = TRUE,
    full.names = TRUE
)
head(jsonfiles, 10)

## Extraire l'info de *tous* les fichiers JSON :
analyticlist <- lapply(jsonfiles, FUN = extract_analytic_info)

## Mesurer le temps d'exécution :
debut <- Sys.time()
analyticlist <- lapply(jsonfiles, FUN = extract_analytic_info)
fin <- Sys.time()
cat("Le traitement complet des fichiers a pris", fin - debut, "secondes.")

## Quel est le type de l'objet R obtenu ?
typeof(analyticlist)

## Afficher les deux premières composantes de cette liste :
analyticlist[[1]]

analyticlist[[2]]

## Inspecter totalement la liste de dataframes :
## str(analyticlist) # attention, la sortie est très longue !

## Transformer cette liste de dataframes en un seul dataframe :
analytic <- do.call(what = rbind, args = analyticlist)
head(analytic)

## Vérifier la dimension du dataframe obtenu :
dim(analytic)

## Est-ce bien la même dimension que la liste initiale ?
length(analyticlist)

## De meilleurs noms de lignes :
rownames(analytic) <- gsub(
    x = jsonfiles,
    pattern = "([[:alnum:]_/-]*)/([[:alnum:]_.]*)(.json)$",
    rep = "\\2"
)
head(analytic)

## Exporter les données vers un fichier csv :
write.csv(analytic, here("data", "data_analytic.csv"), na = "", row.names = TRUE)

analytic |>
    select(name, num_essai, nb_points) |>
    head()

## Remise en forme du dataframe :
cont <- select(analytic, name, num_essai, nb_points) |>
    pivot_wider(names_from = num_essai, values_from = nb_points) |>
    as.data.frame() |>
    column_to_rownames() |>
    mutate_if(is.character, as.numeric) |>
    as.matrix()
head(cont)

## Heatmap interactive :
plot_ly(
    x = rownames(cont),
    y = paste("essai", colnames(cont)),
    z = cont,
    type = "heatmap"
)

## Fonction pour extraire l'information temporelle de chaque fichier :
extract_tempo_info <-  function(file) {
    ## Lire et extraire les info du *contenu* du fichier :
    values <- fromJSON(content = file)
    ## Rassembler les info en un dataframe :
    dtf <- data.frame(routine_time = values$routine_time,
                      pos_ct = values$pos_ct,
                      reco_data = values$reco_data,
                      fb_err = values$fb_err)
    return(dtf)
}

## Appliquer cette fonction à un fichier donné pour exemple :
exmp <- extract_tempo_info(here("data", "sujets", "sujet3", "sujet3_pos_ent_0.2_pc_0.json"))
head(exmp)

summary(exmp)

## Un graphique pour mieux comprendre :
differences <- diff(exmp$routine_time)
options(repr.plot.width = 12, repr.plot.height = 6) # taille du graphique
plot(differences, type = "l", col = "blue", main = "Périodes de temps dans l'expérience")

## Médiane des périodes de temps :
median(differences)

## Extraire les périodes de temps (en ms) :
periods <- round(exmp$routine_time - exmp$routine_time[1], 3)
head(periods)

## Créer un dataframe interpolant les données toutes les 1ms :
frequence <- seq(from = 0, to = max(periods), by = 0.001)
interp <- data.frame(
    time = frequence,
    routine_time = approx(x = periods, y = exmp$routine_time, xout = frequence)$y,
    pos_ct = approx(x = periods, y = exmp$pos_ct, xout = frequence)$y,
    reco_data = approx(x = periods, y = exmp$reco_data, xout = frequence)$y,
    fb_err = approx(x = periods, y = exmp$fb_err, xout = frequence)$y
)
head(interp)
tail(interp)
dim(interp)

dim(exmp)

## Graphiques de contrôle :
par(mfrow = c(2, 1))
plot(pos_ct ~ periods, data = exmp,
     type = "l", col = "blue", main = "Données originelles")
plot(pos_ct ~ time, data = interp,
     type = "l", col = "blue", main = "Données interpolées")

## Retenir un pas de temps de 20 ms :
homog <- subset(interp, time %in% seq(from = 0, to = max(periods), by = 0.02))
head(homog)

## Attribuer de meilleurs rownames (avec des timestamps) :
rownames(homog) <- paste0("2021-07-01 00:00:0", homog$time)
head(homog)

## Ne retenir que les colonnes nécessaires pour la suite :
dat <- homog[, c("pos_ct", "reco_data", "fb_err")]
colnames(dat) <- paste(colnames(dat), "sujet3_pos_ent_0.2_pc_0", sep = "_")
head(dat)

## Procédure complète de lecture, interpolation et homogénéisation d'un fichier JSON :
etf_complete <- function(file) {
    name <- gsub(x = file,
                 pattern = "([-/_[:alnum:]]*)(sujet[[:alnum:]._]*)(.json)",
                 rep = "\\2")
    dtf <- extract_tempo_info(file)
    periods <- round(dtf$routine_time - dtf$routine_time[1], 3)
    frequence <- seq(from = 0, to = 149.955, by = 0.001)
    interp <- data.frame(
        time = frequence,
        routine_time = approx(x = periods, y = dtf$routine_time, xout = frequence)$y,
        pos_ct = approx(x = periods, y = dtf$pos_ct, xout = frequence)$y,
        reco_data = approx(x = periods, y = dtf$reco_data, xout = frequence)$y,
        fb_err = approx(x = periods, y = dtf$fb_err, xout = frequence)$y
    )
    homog <- subset(interp, time %in% seq(from = 0, to = 149.955, by = 0.02))
    rownames(homog) <- paste0("2021-07-01 00:00:0", homog$time)
    homog <- homog[, c("pos_ct", "reco_data", "fb_err")]
    colnames(homog) <- paste(colnames(homog), name, sep = "_")
    return(homog)
}

## On obtient une liste de dataframes temporels (un par individu) :
tempolist <- lapply(jsonfiles, FUN = etf_complete)

## Visualiser le dataframe associé au premier fichier de la liste :
head(tempolist[[1]])

## Concaténer toute la liste de dataframes en un dataframe tempo global :
tempo <- do.call(what = data.frame, args = tempolist)

dim(tempo)
head(tempo[, 1:9])

## Exporter les données temporelles en un fichier csv :
write.csv(tempo, here("data", "data_tempo.csv"), na = "")

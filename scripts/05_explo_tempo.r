## Exécuter à nouveau le notebook 1 :
library(groundhog)
groundhog.library("here", "2021-10-15")
capture.output(source(here("jour2", "01_workflow_jupyter.r")))

## Taille du fichier CSV :
fs::file_info(here("data", "data_tempo.csv"))

## Chargement des données (version fread) :
debut <- Sys.time()
tempo <- fread(
    file = here("data", "data_tempo.csv"),
    sep = ",",
    colClasses = c("character", rep("double", 1320)),
    stringsAsFactors = TRUE
)
fin <- Sys.time()
cat("Le chargement du fichier CSV a pris", fin - debut, "secondes.")
head(tempo[, 1:5])

## Chargement des données (version R-base) :
debut <- Sys.time()
tempo <- read.csv(
    here("data", "data_tempo.csv"),
    row.names = 1,
    stringsAsFactors = TRUE
)
fin <- Sys.time()
cat("Le chargement du fichier CSV a pris", fin - debut, "secondes.")
head(tempo[, 1:4])

dim(tempo)

## Sélection des colonnes "entraînement" uniquement :
colent <- grep(colnames(tempo), pattern = "ent")
ent <- na.omit(tempo[, colent])
dim(ent)

## Observer quelques extraits de ce dataframe d'entraînement :
head(ent[, 1:4], 5)
tail(ent[, 1:4], 5)

## Une grille temporelle :
grille <- seq(from = 0, by = 0.02, length.out = 3000)
head(grille)
tail(grille)

## Sélection des colonnes "test" uniquement :
coltest <- grep(colnames(tempo), pattern = "test")
test <- na.omit(tempo[, coltest])
dim(test)

## Observer quelques extraits de ce dataframe de test :
head(test[, 1:4], 5)
tail(test[, 1:4], 5)

## Une grille temporelle :
grille <- seq(from = 0, by = 0.02, length.out = 7498)
head(grille)
tail(grille)

## Constuction d'un dtf tempo plus homogène :
tempo <- tempo[rownames(test), ]
dim(tempo)

## Création de sous-ensembles de données :
reco <- tempo[, grep(colnames(tempo), pattern = "reco_data")]
pos <- tempo[, grep(colnames(tempo), pattern = "pos_ct")]
fbe <- tempo[, grep(colnames(tempo), pattern = "fb_err")]

dim(reco)
dim(pos)
dim(fbe)

cdif <- reco - pos
head(cdif - fbe)

## Quelle est la différence maximale entre ces deux modes de l'erreur ?
max(cdif - fbe, na.rm = TRUE)

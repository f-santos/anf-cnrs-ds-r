#+TITLE: Un exemple d'utilisation d'Org mode pour la programmation lettrée
#+AUTHOR: Frédéric Santos
#+DATE: 25 novembre 2021
#+EMAIL: frederic.santos@u-bordeaux.fr
#+STARTUP: showall
#+OPTIONS: email:t toc:t ^:nil
#+LATEX_HEADER: \usepackage[natbibapa]{apacite}
#+LATEX_HEADER: \usepackage[french]{babel}
#+LATEX_HEADER: \usepackage{a4wide}
#+LATEX_HEADER: \usepackage{mathpazo}
#+LATEX_HEADER: \usepackage[matha,mathb]{mathabx}
#+LATEX_HEADER: \usepackage{amsmath}
#+LATEX_HEADER: \usepackage{titlesec}
#+LATEX_HEADER: \titlelabel{\thetitle.\quad}
#+LATEX_HEADER: \usepackage[usenames,dvipsnames]{xcolor} % For colors with friendly names
#+LATEX_HEADER: \usepackage{minted}
#+LATEX_HEADER: \usepackage{mdframed}                    % Companion of minted for code blocks
#+LATEX_HEADER: \usepackage{fancyvrb}                    % For verbatim outputs
#+LATEX_HEADER: \usepackage{fvextra}                     % For verbatim outputs
#+LATEX_HEADER: \usemintedstyle{friendly} % set style if needed, see https://frama.link/jfRr8Lpj
#+LATEX_HEADER: \mdfdefinestyle{mystyle}{linecolor=gray!30,backgroundcolor=gray!30}
#+LATEX_HEADER: \BeforeBeginEnvironment{minted}{%
#+LATEX_HEADER: \begin{mdframed}[style=mystyle]}
#+LATEX_HEADER: \AfterEndEnvironment{minted}{%
#+LATEX_HEADER: \end{mdframed}}
#+LATEX_HEADER: %% Formatting of verbatim outputs (i.e., outputs of R results):
#+LATEX_HEADER: \DefineVerbatimEnvironment{verbatim}{Verbatim}{%
#+LATEX_HEADER:   fontsize = \small,
#+LATEX_HEADER:   frame = leftline,
#+LATEX_HEADER:   formatcom = {\color{gray!97}},
#+LATEX_HEADER:   breaklines=true
#+LATEX_HEADER: }
#+LATEX_HEADER: \usepackage{float}
#+LATEX_HEADER: \usepackage{url}
#+LATEX_HEADER: %% For DOI hyperlinks in biblio:
#+LATEX_HEADER: \usepackage{doi}
#+LATEX_HEADER: \renewcommand{\doiprefix}{}
#+PROPERTY: header-args :tangle yes
#+BIBLIOGRAPHY: ~/PACEA_MyCore/complete_biblio.bib
#+CITE_EXPORT: natbib apacite
#+LANGUAGE: fr

#+begin_export latex
\pagebreak
#+end_export

* Brève présentation
** Introduction générale
Org mode est un mode majeur de l'éditeur de texte Emacs. Créé au début des années 2000 par l'astronome Carsten Dominik, il présente deux facettes principales :
- outil de prise de notes et de planification de tâches utilisant une syntaxe légère ;
- système de programmation lettrée similaire à Jupyter et Rmarkdown.

Org mode possède une documentation "papier" fournie [cite:@dominik2010_OrgModeReference], ainsi qu'un site web comportant de nombreuses informations complémentaires : [[https://orgmode.org/][https://orgmode.org/]].

** Quelques avantages
- Org mode travaille sur des fichiers /intégralement en texte brut/, donc aisés à versionner, et offrant un système léger et durable.
- Org mode permet de profiter de tout l'écosystème de l'éditeur de texte Emacs : un gestionnaire de notes bibliographiques (Org-Roam), des fonctionnalités étendues d'édition LaTeX (AUCTEX), un environnement de développement intégré pour R (ESS, Emacs Speaks Statistics), etc.
- Org Babel (le versant "programmation lettrée" d'Org mode) prend nativement en charge près de 50 langages de programmation, et permet très aisément de faire passer des variables d'un langage vers l'autre.
- La syntaxe Org est très légère et offre des fonctionnalités inédites et plus avancées que celle de Markdown (édition de tables, références croisées facilitées, gestion native de références bibliographiques, etc.).
- Une communauté d'utilisateurs très active, notamment sur StackOverflow, Reddit, Telegram et IRC.

** Les inconvénients
Org mode nécessite un peu plus de temps d'apprentissage que les solutions concurrentes. Toutefois, de nombreux tutoriels Youtube sont là pour vous assister, et même un MOOC !

#+begin_center
[[https://www.udemy.com/course/getting-yourself-organized-with-org-mode/][https://www.udemy.com/course/getting-yourself-organized-with-org-mode/]]
#+end_center

* Programmation lettrée : un exemple pratique
Montrons ci-dessous comment fonctionne Org Babel, et en particulier comment faire interagir R et Python à travers cette interface.

** Préparer un dataframe avec R
On commence par charger le dataframe =iris= en R, puis par en sélectionner un sous-ensemble de seulement 15 fleurs (sur les 150 fleurs originellement présentes).

#+NAME: data-iris
#+begin_src R :results value table :session *R* :exports both :colnames yes
data(iris)
## Ne retenir que 15 fleurs (5 de chaque espèce) :
dat <- iris[c(1:5, 51:55, 101:105), ]
print(dat)
#+end_src

#+CAPTION: Un extrait du dataframe =iris= généré avec R.
#+RESULTS: data-iris
| Sepal.Length | Sepal.Width | Petal.Length | Petal.Width | Species    |
|--------------+-------------+--------------+-------------+------------|
|          5.1 |         3.5 |          1.4 |         0.2 | setosa     |
|          4.9 |           3 |          1.4 |         0.2 | setosa     |
|          4.7 |         3.2 |          1.3 |         0.2 | setosa     |
|          4.6 |         3.1 |          1.5 |         0.2 | setosa     |
|            5 |         3.6 |          1.4 |         0.2 | setosa     |
|            7 |         3.2 |          4.7 |         1.4 | versicolor |
|          6.4 |         3.2 |          4.5 |         1.5 | versicolor |
|          6.9 |         3.1 |          4.9 |         1.5 | versicolor |
|          5.5 |         2.3 |            4 |         1.3 | versicolor |
|          6.5 |         2.8 |          4.6 |         1.5 | versicolor |
|          6.3 |         3.3 |            6 |         2.5 | virginica  |
|          5.8 |         2.7 |          5.1 |         1.9 | virginica  |
|          7.1 |           3 |          5.9 |         2.1 | virginica  |
|          6.3 |         2.9 |          5.6 |         1.8 | virginica  |
|          6.5 |           3 |          5.8 |         2.2 | virginica  |

** Le transférer vers Python
*** Première tentative ("naïve")
Si on essaie de passer ce dataframe en Python, il sera nativement reconnu comme /liste/ par Python (ce qui n'est pas anormal, étant donné que la classe ~data.frame~ en R n'est rien d'autre qu'une surcouche du mode fondamental ~list~) :

#+begin_src python :results output :session :exports both :var rdata=data-iris :colnames no
## En Python...
print(type(rdata))              # on obtient une liste
#+end_src

#+RESULTS:
: <class 'list'>

Pour s'en convaincre, on peut carrément afficher le contenu de cette liste :
#+begin_src python :results verbatim output :session :exports both
print(rdata)
#+end_src

#+RESULTS:
: [['Sepal.Length', 'Sepal.Width', 'Petal.Length', 'Petal.Width', 'Species'], [5.1, 3.5, 1.4, 0.2, 'setosa'], [4.9, 3, 1.4, 0.2, 'setosa'], [4.7, 3.2, 1.3, 0.2, 'setosa'], [4.6, 3.1, 1.5, 0.2, 'setosa'], [5, 3.6, 1.4, 0.2, 'setosa'], [7, 3.2, 4.7, 1.4, 'versicolor'], [6.4, 3.2, 4.5, 1.5, 'versicolor'], [6.9, 3.1, 4.9, 1.5, 'versicolor'], [5.5, 2.3, 4, 1.3, 'versicolor'], [6.5, 2.8, 4.6, 1.5, 'versicolor'], [6.3, 3.3, 6, 2.5, 'virginica'], [5.8, 2.7, 5.1, 1.9, 'virginica'], [7.1, 3, 5.9, 2.1, 'virginica'], [6.3, 2.9, 5.6, 1.8, 'virginica'], [6.5, 3, 5.8, 2.2, 'virginica']]

*** "La" solution
En Python, le type dataframe est géré par la librairie ~pandas~ : il suffit simplement de convertir cette liste en dataframe grâce à cette librairie: 

#+begin_src python :results value :session :exports both
import pandas as pd
df = pd.DataFrame(rdata[1:], columns=rdata[0])
df.head()
#+end_src

#+RESULTS:
:    Sepal.Length  Sepal.Width  Petal.Length  Petal.Width Species
: 0           5.1          3.5           1.4          0.2  setosa
: 1           4.9          3.0           1.4          0.2  setosa
: 2           4.7          3.2           1.3          0.2  setosa
: 3           4.6          3.1           1.5          0.2  setosa
: 4           5.0          3.6           1.4          0.2  setosa

Et voilà !

** Utilisation de ce dataframe en Python
On peut tracer un nuage de points (Fig. [[biplot-iris]]) croisant largeur et longueur des pétales pour prouver que ce dataframe a correctement été pris en compte par Python :

#+begin_src python :results file :session :exports results
import matplotlib.pyplot as plt
plt.scatter(df["Petal.Length"], df["Petal.Width"], c='purple')
plt.savefig("biplot_iris.png")
"biplot_iris.png"
#+end_src

#+NAME: biplot-iris
#+CAPTION: Un exemple de nuage de points généré avec Python.
#+RESULTS:
[[file:biplot_iris.png]]

* Biblio :ignore:
#+print_bibliography:
